package kz.attractor.exam9.Controller;

import kz.attractor.exam9.dto.PlaceDTO;
import kz.attractor.exam9.dto.PlaceForm;
import kz.attractor.exam9.model.Place;
import kz.attractor.exam9.service.*;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Principal;


@Controller
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class MainController {
    PlaceService placeService;
    CustomerService customerService;
    RatingService ratingService;
    PropertiesService ps;
    CommentService commentService;


    @GetMapping
    public String indexPage(Model model, HttpServletRequest uriBuilder, Pageable pageable) {
        var uri = uriBuilder.getRequestURI();
        var posts = placeService.getPlaces(pageable);

        constructPageable(posts,ps.getDefaultPageSize(),model,uri);

        return "main";
    }

    @GetMapping("/find/{find}")
    public List<PlaceDTO> find(
            @PathVariable("find")
                    String find, Pageable pageable){
        return placeService.getFindProduct(find,pageable).getContent();
    }

    @GetMapping("/findPlaces/{text-to-search}")
    @ResponseBody
    public List<PlaceDTO> findPlaces(@PathVariable("text-to-search") String textToSearch){
        return placeService.findPlaces(textToSearch);
    }

    @GetMapping("/place/{id}")
    public String getPost(@PathVariable("id") int id,Authentication authentication,Model model,Pageable pageable,HttpServletRequest uriBuilder){

        var place = placeService.getPost(id,authentication);
        var uri = uriBuilder.getRequestURI();
        var comments = commentService.allComments(id,pageable);

        constructPageable(comments,ps.getDefaultPageSize(),model,uri);
        model.addAttribute("post", place);

        return "only_theme";
    }

    @GetMapping("/newPlace")
    public String newTheme(){

        return "newTheme";
    }

    @PostMapping("/newPlace")
    public String newPost(@RequestParam("text")String text, Authentication authentication, @RequestParam("name")String name) {
//        File imgFile = new File("src/main/resources/static/images/" + image.getOriginalFilename());
//        File imgTarget = new File("target/classes/static/images/" + image.getOriginalFilename());
//        FileOutputStream o = new FileOutputStream(imgFile);
//        FileOutputStream o2 = new FileOutputStream(imgTarget);
//        o.write(image.getBytes());
//        o2.write(image.getBytes());
//        o.close();
//        o2.close();

        placeService.savePlace(authentication,text,name);
        return "redirect:/";
    }

    private static <T> void constructPageable(Page<T> list, int pageSize, Model model, String uri) {
        if (list.hasNext()) {
            model.addAttribute("nextPageLink", constructPageUri(uri, list.nextPageable().getPageNumber(), list.nextPageable().getPageSize()));
        }

        if (list.hasPrevious()) {
            model.addAttribute("prevPageLink", constructPageUri(uri, list.previousPageable().getPageNumber(), list.previousPageable().getPageSize()));
        }

        model.addAttribute("hasNext", list.hasNext());
        model.addAttribute("hasPrev", list.hasPrevious());
        model.addAttribute("items", list.getContent());
        model.addAttribute("defaultPageSize", pageSize);
    }
    private static String constructPageUri(String uri, int page, int size) {
        return String.format("%s?page=%s&size=%s", uri, page, size);
    }


    @PostMapping("/newComment")
    public String newComment(@RequestParam("text") String text, @RequestParam("id")int id, Authentication authentication){
        commentService.newComment(text, authentication, id);
        return "redirect:/place/"+id;
    }
//    @GetMapping
//    public String getPlace(Model model, @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable, Authentication authentication, HttpServletRequest uriBuilder) {
//        Page<PlaceDTO> listPlace = placeService.getPlaces(pageable);
//        var uri = uriBuilder.getRequestURI();
//        model.addAttribute("url", uri);
//        try {
//            var customer = customerService.getByEmail(authentication.getName());
//            model.addAttribute("user", customer);
//        } catch (NullPointerException n) {
//            model.addAttribute("nullUser", "nullUser");
//        }
//        customerService.addAuthentication(model, authentication);
//
//        model.addAttribute("page", listPlace);
//        model.addAttribute("url", "/");
//        model.addAttribute("places", placeService.getPlaces(pageable));
//        model.addAttribute("placeForm", new PlaceForm());
//        model.addAttribute("ratingTypes", ratingService.findAllRatings());
//        return "main";
//    }

//    @RequestMapping(value = "/get_place", method = RequestMethod.POST)
//    public String addPlace(@Valid PlaceForm placeForm, BindingResult validationResult, RedirectAttributes attributes) {
//
//        attributes.addFlashAttribute("placeForm", placeForm);
//
//        if (validationResult.hasFieldErrors()) {
//            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
//            return "redirect:/get_place";
//        }
//
//            Optional<Place> getUpdateDetail = placeService.getPlaceById(placeForm.getPlace_id());
//
//            if (getUpdateDetail.isEmpty()) {
//                placeService.savePlace(placeForm);
//            }
//            else {
//                placeService.updatePlace(getUpdateDetail, placeForm);
//            }
//        return "redirect:/";
//    }
}
