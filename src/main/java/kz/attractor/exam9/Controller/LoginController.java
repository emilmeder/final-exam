package kz.attractor.exam9.Controller;

import kz.attractor.exam9.model.CustomerRegisterForm;
import kz.attractor.exam9.service.CustomerService;
import kz.attractor.exam9.service.PropertiesService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class LoginController {
    private final CustomerService cs;

    @GetMapping("/registration")
    public String pageRegisterCustomer(Model model) {
        if (!model.containsAttribute("dto")) {
            model.addAttribute("dto", new CustomerRegisterForm());
        }
        return "registration";
    }

    @PostMapping("/registration")
    public String registerPage(@Valid CustomerRegisterForm customerRequestDto, BindingResult validationResult, RedirectAttributes attributes) {
        attributes.addFlashAttribute("dto", customerRequestDto);

        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/registration";
        }

        cs.register(customerRequestDto);
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String loginPage(@RequestParam(required = false, defaultValue = "false") Boolean error, Model model) {
        model.addAttribute("error", error);
        return "login";
    }

    @GetMapping("/profile")
    public String getProfile(Authentication authentication,Model model){

        var user = cs.getLoginCustomer(authentication);
        model.addAttribute("customer",user);

        return "profile";
    }
    @GetMapping("/logout")
    public String invalidate(HttpSession session) {
        if (session != null) {
            session.invalidate();
        }

        return "redirect:/";
    }

//
//    @GetMapping("/posts/{id}")
//    public String getPost(@PathVariable("id") int id,Authentication authentication,Model model,Pageable pageable,HttpServletRequest uriBuilder){
//
//        var post = ps.getPost(id,authentication);
//        var uri = uriBuilder.getRequestURI();
//        var comments = commentService.allComments(id,pageable);
//
//        constructPageable(comments,propertiesService.getDefaultPageSize(),model,uri);
//        model.addAttribute("post", post);
//
//        return "only_theme";
//    }

//    @PostMapping("/newComment")
//    public String newComment(@RequestParam("text") String text, @RequestParam("id")int id, Authentication authentication){
//        commentService.newComment(text, authentication, id);
//        return "redirect:/posts/"+id;
//    }
}
