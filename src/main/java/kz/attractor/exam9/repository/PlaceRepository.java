package kz.attractor.exam9.repository;

import kz.attractor.exam9.model.Place;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaceRepository extends JpaRepository<Place, Integer> {
    Page<Place> findPlaceByName(String name, Pageable pageable);
}
