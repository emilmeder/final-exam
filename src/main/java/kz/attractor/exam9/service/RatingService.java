package kz.attractor.exam9.service;

import kz.attractor.exam9.dto.RatingDTO;
import kz.attractor.exam9.model.Rating;
import kz.attractor.exam9.repository.RatingRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class RatingService {

    private RatingRepository rr;

    public List<Rating> findAllRatings(){
        return rr.findAll();
    }

}
