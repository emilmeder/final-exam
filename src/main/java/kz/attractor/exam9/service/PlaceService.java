package kz.attractor.exam9.service;

import kz.attractor.exam9.dto.PlaceDTO;
import kz.attractor.exam9.dto.PlaceForm;
import kz.attractor.exam9.model.Place;
import kz.attractor.exam9.repository.CustomerRepository;
import kz.attractor.exam9.repository.PlaceRepository;
import kz.attractor.exam9.repository.RatingRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PlaceService {
    PlaceRepository placeRepository;
    CustomerRepository customerRepository;
    RatingRepository ratingRepository;

    public void savePlace(Authentication authentication, String text, String name){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        var user = customerRepository.findByEmail(userDetails.getUsername());
        var place = Place.builder()
                .text(text).customer(user)
                .date(LocalDate.now())
                .name(name)
//                .image(image)
                .build();

        placeRepository.save(place);
    }

    public Page<PlaceDTO> getFindProduct(String name,Pageable pageable){
        return placeRepository.findPlaceByName(name, pageable)
                .map(PlaceDTO::from);
    }


    public void updatePlace(Optional<Place> getUpdateDetail, PlaceForm placeForm ) {
        Place place = getUpdateDetail.get();
        place.setRating(ratingRepository.findById(placeForm.getRating_id()).get());
            placeRepository.save(place);

    }


    public Optional<Place> getPlaceById(int id) {
        return placeRepository.findById(id);
    }

    public Page<PlaceDTO> getPlaces(Pageable pageable){
        return placeRepository.findAll(pageable).map(PlaceDTO::from);
    }

    public PlaceDTO getPost(int id, Authentication authentication){
        return PlaceDTO.from(placeRepository.findById(id).get());
    }

    public List<PlaceDTO> findPlaces(String textToSearch){
        List<Place> artworks = placeRepository.findAll();
        List<PlaceDTO> placeDTO = new ArrayList<>();
        for(Place a: artworks){
            if(a.getName().toLowerCase().contains(textToSearch.toLowerCase())){
                placeDTO.add(PlaceDTO.from(a));
            }
        }
        return placeDTO;
    }

    }

