package kz.attractor.exam9.service;


import kz.attractor.exam9.dto.CommentDTO;
import kz.attractor.exam9.model.Comment;

import kz.attractor.exam9.repository.CommentRepository;
import kz.attractor.exam9.repository.CustomerRepository;
import kz.attractor.exam9.repository.PlaceRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDate;

@Service
@AllArgsConstructor
public class CommentService {
    private final CommentRepository commentRepository;
    private final CustomerRepository customerRepository;
    private final PlaceRepository placeRepository;


    public Page<CommentDTO> allComments(int id, Pageable pageable){
        return commentRepository.findAllByPlaceId(id,pageable).map(CommentDTO::from);
    }

    public void newComment(String text, Authentication authentication,int id){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        var user = customerRepository.findByEmail(userDetails.getUsername());
        var comment = Comment.builder().customer(user).date(LocalDate.now()).place(placeRepository.findById(id).get()).text(text).build();
        commentRepository.save(comment);

    }
}
