package kz.attractor.exam9.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Table(name = "images")
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String image;

    @ManyToOne
    @JoinColumn(name = "place_id")
    private Place place;


}
