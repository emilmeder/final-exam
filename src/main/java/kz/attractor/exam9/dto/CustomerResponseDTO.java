package kz.attractor.exam9.dto;


import kz.attractor.exam9.model.Customer;
import lombok.*;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(access = AccessLevel.PACKAGE)
@ToString
public class CustomerResponseDTO {

    private int id;
    private String fullname;
    private String email;


    public static CustomerResponseDTO from(Customer user) {

        return builder()
        .id(user.getId())
        .fullname(user.getFullname())
        .email(user.getEmail())
        .build();
    }
}
