package kz.attractor.exam9.dto;

import kz.attractor.exam9.model.Customer;
import kz.attractor.exam9.model.Place;
import kz.attractor.exam9.model.Rating;
import lombok.*;

import java.time.LocalDate;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(access = AccessLevel.PRIVATE)
public class RatingDTO {
    private int id;
    private int count;
    private Customer customer;

    public static RatingDTO from(Rating rating) {
        return builder()
                .id(rating.getId())

                .customer(rating.getCustomer())
                .build();
    }
}