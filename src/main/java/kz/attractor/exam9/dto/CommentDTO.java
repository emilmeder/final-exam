package kz.attractor.exam9.dto;


import kz.attractor.exam9.model.Comment;
import kz.attractor.exam9.model.Customer;
import kz.attractor.exam9.model.Place;
import kz.attractor.exam9.model.Rating;
import lombok.*;

import java.sql.Timestamp;
import java.time.LocalDate;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(access = AccessLevel.PRIVATE)
public class CommentDTO {
    private int id;
    private String text;
    private LocalDate date;
    private Customer customer;
    private Place place;

    public static CommentDTO from(Comment comment) {
        return builder()
                .id(comment.getId())
                .text(comment.getText())
                .date(comment.getDate())
                .customer(comment.getCustomer())
                .place(comment.getPlace())
                .build();
    }
}
