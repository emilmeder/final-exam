package kz.attractor.exam9.dto;


import kz.attractor.exam9.model.Customer;
import lombok.*;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(access = AccessLevel.PACKAGE)
@ToString
public class CustomerDTO {
    private int id;
    private String email;
    private String fullname;
    private boolean enabled;
    private String role;

    public static CustomerDTO from(Customer customer){
        return builder()
                .email(customer.getEmail())
                .fullname(customer.getFullname())
                .id(customer.getId())
                .enabled(true)
                .role(customer.getRole())
                .build();
    }
}

