package kz.attractor.exam9.dto;

import kz.attractor.exam9.model.Customer;
import kz.attractor.exam9.model.Place;
import kz.attractor.exam9.model.Rating;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(access = AccessLevel.PRIVATE)
public class PlaceDTO {
    private int id;
    private String name;
    private LocalDate date;
    private String text;
    private String image;
    private Customer customer;
    private Rating rating;

    public static PlaceDTO from(Place place) {
        return builder()
                .id(place.getId())
                .name(place.getName())
                .date(place.getDate())
                .text(place.getText())
                .customer(place.getCustomer())
                .rating(place.getRating())
                .image(place.getImage())
                .build();
    }
}
