package kz.attractor.exam9.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;

@Getter
@Setter
public class PlaceForm {

    private int place_id;
    private String date;
    private String name;
    private int rating_id;

    private int customer_id;

}