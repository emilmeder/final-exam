use `exam`;

CREATE TABLE `customers` (
     `id` INT NOT NULL AUTO_INCREMENT,
     `email` VARCHAR(128) NOT NULL,
     `password` VARCHAR(128) NOT NULL,
     `fullname` VARCHAR(128) NOT NULL,
     `enabled` boolean NOT NULL,
     `role` VARCHAR(16) NOT NULL,
     PRIMARY KEY (`id`)
);
CREATE TABLE `rating` (
                          `id` INT NOT NULL AUTO_INCREMENT,
                          `count` VARCHAR(50),
                          `customers_id` INT,
                          PRIMARY KEY (`id`),
                          CONSTRAINT `fk_rating_customers1`
                              FOREIGN KEY (`customers_id`)REFERENCES `customers` (`id`)
);

CREATE TABLE `places` (
     `id` INT NOT NULL AUTO_INCREMENT,
     `name` VARCHAR(50),
     `text` VARCHAR(300),
     `date` date,
     `customers_id` INT,
     `rating_id` INT,
     `image` VARCHAR(300),
     PRIMARY KEY (`id`),
     CONSTRAINT `fk_posts_customers` FOREIGN KEY (`customers_id`)REFERENCES `customers` (`id`),
     CONSTRAINT `fk_posts_rating` FOREIGN KEY (`rating_id`)REFERENCES `rating` (`id`)
);

CREATE TABLE `comments` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `text` VARCHAR(50) NULL,
    `date` date,
    `place_id` INT NOT NULL,
    `customers_id` INT NOT NULL,
    PRIMARY KEY (`id`, `place_id`, `customers_id`),
    CONSTRAINT `fk_comments_place_id`
        FOREIGN KEY (`place_id`)REFERENCES `places` (`id`),
    CONSTRAINT `fk_comments_customers1`
        FOREIGN KEY (`customers_id`)REFERENCES `customers` (`id`)
);

CREATE TABLE `images`
(
    `id`       INT          NOT NULL AUTO_INCREMENT,
    `image`    VARCHAR(255),
    `place_id` INT,

    PRIMARY KEY (`id`),
    CONSTRAINT `place_fk` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`)
);


insert into `customers` (email, password, fullname, enabled, role) VALUES
('Johny@mail.ru','123','Johny-3',true,'USER'),
('Johny@mail.ru','123','Johny-Scene',true,'USER'),
('Johny@mail.ru','123','Johny',true,'USER'),
('Johny@mail.ru','123','J-Dog',true,'USER');

insert into `places` (name, text, date, customers_id, image) VALUES
('Moschino','Some long and useless text','2018-12-12', 1, '5'),
('Belajo','Some long and useless text','2018-12-12', 2, '/images/5.jpg'),
('Sigma','Some long and useless text','2018-12-12', 1, '/images/5.jpg'),
('Taylor','Some long and useless text','2018-12-12', 3, '/images/5.jpg'),
('Yamaha','Some long and useless text','2018-12-12', 4, '/images/5.jpg'),
('Gibson','Some long and useless text','2018-12-12', 4, '/images/5.jpg'),
('Martin','Some long and useless text','2018-12-12', 2, '/images/5.jpg');


insert into `rating` (count) VALUES
('1'),
('2'),
('3'),
('4'),
('5');



