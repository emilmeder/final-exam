'use strict';

async function findArtworks(form) {
    let data = new FormData(form);
    let textToSearch = data.get("text-to-search").toString();
    let response = await fetch("http://localhost:8080/findPlaces/"+textToSearch);
    return await response.json();
}


function find() {
    let value = document.getElementById("finder").value;
    if(value.length === 0){
        return;
    }

    else{
        window.location.href = 'http://localhost:8080/find/'+value;
    }
};